// When the DOM is ready, run this function
$(document).ready(function() {
  //Set the carousel options
  $('#quote-carousel').carousel({
    pause: true,
    interval: false,
  });
});



function submitContactForm(event, $form) {
	event.preventDefault();
	
	$(".has-error", $form).removeClass("has-error");
	
	$.post("sendmail.php", $form.serialize(), function(json) {
		
		if(!json.status) {
			$('[type="submit"]', $form).button("reset").val("Wyślij");
			
			for(i=0; i<json.errors.length; i++) {
				$group = $( $("[name='"+json.errors[i].class+"']", $form).parents('.form-group, .adresaci')[0] );
				$(".text-danger", $group).text(json.errors[i].msg);
				$group.addClass("has-error");
			}
		}
		else {
			alert('Wiadomość została wysłana');
		}
	});
}

$(function() {
	$(".contact-info form").submit( function(e){submitContactForm(e, $(this) ) } );
});



function submitOrderForm(event, $form) {
	event.preventDefault();
	
	$(".has-error", $form).removeClass("has-error");
	
	$.post("sendorder.php", $form.serialize(), function(json) {
		
		if(!json.status) {
			$('[type="submit"]', $form).button("reset").val("Wyślij");
			
			for(i=0; i<json.errors.length; i++) {
				$group = $( $("[name='"+json.errors[i].class+"']", $form).parents('.form-group, .adresaci')[0] );
				$(".text-danger.small", $group).text(json.errors[i].msg);
				$group.addClass("has-error");
			}
		}
		else {
			alert('Zamówienie zostało wysłane');
		}
	});
}

$(function() {
	$(".order form").submit( function(e){submitOrderForm(e, $(this) ) } );
});
