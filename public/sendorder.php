<?php
header('Content-Type: application/json');

/**
 * POST:
 * - name
 * - email
 * - phone
 * - text
 */


$errors = array();
if(!isset($_POST['name']) || !$_POST['name']) {
    $errors[] = array('class'=>'name', 'msg'=>'Podaj imię i nazwisko');
}

if(!isset($_POST['email']) || !$_POST['email']) {
    $errors[] = array('class'=>'email', 'msg'=>'Podaj adres email');
}
if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $errors[] = array('class'=>'email', 'msg'=>'Nieprawidłowy adres');
}

if(!isset($_POST['pwz']) || !$_POST['pwz']) {
    $errors[] = array('class'=>'pwz', 'msg'=>'Podaj numer prawa wykonywania zawodu');
}

if(sizeof($errors)) {
    echo json_encode( array('status'=>0, 'errors'=>$errors), true );
    exit;
}




$body = "Zamawiam dostęp do programu Vetoteka.

Imię i nazwisko:      {$_POST['name']}
Adres e-mail:  {$_POST['email']}
Numer PWZ: {$_POST['pwz']}";

$headers  = "From: {$_POST['name']} <{$_POST['email']}>\n";
$headers .= "Reply-To: {$_POST['name']} <{$_POST['email']}>\n";
$headers .= 'Content-type: text/plain; charset=UTF-8' . "\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-Transfer-Encoding: 8bit\n";
$headers .= "X-Mailer: PHP\n";


$subject = "=?UTF-8?B?".base64_encode("Zamówienie dostępu do Vetoteki")."?=";
$toMail = 'kontakt@vetoteka.pl';


mail($toMail, $subject, $body, $headers);


echo json_encode( array('status'=>1) );

